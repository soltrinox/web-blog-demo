Web blog
========

This is a simple web blog to demo use cases with Hashicorp Vault. The app had multiple phases of development. 
- In the first phase it pulled the mongoDB creds from a simple `.env` configuration file. The issue with that is that I couldn't push `.env` file into VCS. 
- In the second phase it used a KV secret for the mongoDB creds. The KV secret was automatically injected by the Vault-agent sidecar at the `/app/secrets/.envapp` then the app pulled the secrets from there. The app is still unaware of Vault at this point. 
- In the third phase, the app communicates directly with Vault using the API to obtain dynamic secrets for mongoDB. 
- In the fourth phase, the app still uses dynamic secrets but it also encrypts the content of the posts by using Vault's transit engine.

The app uses python, flask, bootstrap, mongodb, consul, and vault.

# GKE

## Create a global static IP to be used with the Ingress

```shell
gcloud compute addresses create webblog-ip --global
```

To find the static IP you created:

```shell
gcloud compute addresses describe webblog-ip --global
```

## Ingress

I'm using GCE's built in ingress so I didn't need to install anything as it comes built in with the cluster. However, you need to use a NodePort with the service that the Ingress will point it's backend to. I've also specified the static `webblog-ip` to be used with the ingress so I can point a DNS entry to it.

# [MongoDB Setup](https://docs.mongodb.com/manual/tutorial/enable-authentication/)

## MongoDB Helm Chart

Use the following for mongoDB helm chart
https://github.com/helm/charts/tree/master/stable/mongodb

Use this command to install:

```shell
kubectl create ns webblog
helm3 install webblog --namespace webblog -f K8s/values-mongo.yaml stable/mongodb
```

## Upgrading the MongoDB helm chart

```shell
helm3 upgrade webblog -f K8s/values-mongo.yaml stable/mongodb
```

Follow the helm instructions to get the root password to connect to the DB (this is also specified in the values-mongo file)

## To access the mongoDB client

Exec into the mongodb pod and run the following command

```shell
mongo --port 27017  --authenticationDatabase "admin" -u "root" -p "xxxxx"
```

## Migrate a DB in mongoDB

### Get the DB

mongodump --db=webblogencrypted --authenticationDatabase "admin" --username="root" --password="xxxx" --out=<a-path-on-your-computer>
kubectl cp <pod-name>:/tmp/webblog-20200326 <a-path-on-your-computer>

### Restore the DB

kubectl cp <a-path-on-your-computer> <pod-name>:/tmp/webblog-20200326

mongorestore --db=webblogencrypted --authenticationDatabase "admin" --username="root" --password="xxxx" /tmp/webblog-20200326/webblogencrypted


# [Vault setup](https://www.vaultproject.io/docs/secrets/databases/mongodb.html)

## Vault config

Important information regarding agent annotations. Remember to use vault.hashicorp.com/agent-pre-populate-only annotation for cronjob or else the vault-agent remains up and the pods remain up and continue to pile up and jobs don't finish.
https://www.vaultproject.io/docs/platform/k8s/injector/annotations/#inlinecode-vaulthashicorpcomagent-pre-populate-only-2


Make sure to run the following command inside the vault container to properly set up the K8s authentication backend
```shell
vault auth enable kubernetes
vault write auth/kubernetes/config \
        token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
        kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
        kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
```

## A VERY IMPORTANT command to give the correct API call is found below:

```shell
vault kv get -output-curl-string internal/webblog/mongodb
curl -H "X-Vault-Request: true" -H "X-Vault-Token: $(vault print token)" http://vault.vault.svc.cluster.local:8200/v1/internal/data/webblog/mongodb
```

# Demo Steps

0. Make sure you're logged out of the app. Have the VS code screen with the teriminal output showing. Also have the VS code screen side by side to the Chrome screen.
1. Comment and uncomment the lines in `databse.py` and `.env` to show the static hard-coded creds scenario.
2. Log into the app
3. Show the stdout in VS code's terminal showing the hard-coded username and password are the same as those in the `.env` file.
4. Browse the Blogs page to show that the creds don't expire.
5. Log out of the app
6. Comment and uncomment the lines in `databse.py` and `.env` to show the dynamic secrets scenario using Vault
7. Log into the app
8. Show the stdout in VS code's terminal showing the auto-generated username and password by vault.
9. Browse the Blogs page to show that the creds expire and we get a message saying `mongoDB auth failed due to creds expiring. Rotating creds now`. Then we get new creds.
10. Pivot over to a terminal with the mongo client running. Make sure you're logged in as an admin. Run the commands: `use admin` and `show users`. Show how the creds appear and disappear based on the timeout. You will need to browse the web blog to generate new creds.


## Waypoint Deploy

### Install the Waypoint Server in K8s

Create a waypoint Namespace
```shell
kubectl create ns waypoint
```

Install the Waypoint Server
```shell
waypoint install --platform=kubernetes -accept-tos -k8s-namespace=waypoint
```

Check the UI by going to the Web UI Address given

Create waypoint token
```shell
waypoint token new
```

### Build Deploy Release with waypoint up

```shell
waypoint init
waypoint up
```

### Using GitLab CI/CD

All you need to do is use the appropriate `.gitlab-ci.yml` file and commit and push. This will create a docker container with waypoint in it and will use the WSL runner specified in my tags.

### Demo Steps

#### Scenario 1 - Manual - No CI/CD Tools
Commit: git commit -am 'Scenario 1 - run' 
Push: git push
Docker Build: docker build -t registry.gitlab.com/public-projects3/web-blog-demo .
Docker Push: docker push registry.gitlab.com/public-projects3/web-blog-demo
Kubectl Apply: kubectl apply -f app/K8s/app_deploy_manual.yaml

#### Scenario 2 - Waypoint - No CI/CD Tools
Commit: git commit -am 'Scenario 2 - run' 
Push: git push
Waypoint: waypoint up

#### Scenario 3 - GitLab CI/CD - No Waypoint
Commit: git commit -am 'Scenario 3 - run' 
Push: git push

#### Scenario 4 - GitLab CI/CD - With Waypoint
Commit: git commit -am 'Scenario 4 - run' 
Push: git push
